<div id=site_logo>
	<h1><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?> <?php _e('home page'); ?>"><?php bloginfo('name'); ?></a></h1>
	<?php bloginfo('description'); ?>

	<div id="search">
		<?php include (TEMPLATEPATH . '/search-bar.php'); ?>
	</div>
</div>

<div id ="pages">
	<ul>
		<?php wp_list_pages('title_li=')?>
	</ul>
</div>

