<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<!--need to change this back to having slash at front-->
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />

		<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/wordpress-defaults.css">

	</head>

	<body>
		<header>
			<?php get_header(); ?>
		</header>

		<div id="main">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<h1><a class="postLink" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
					<!--<h4>Posted on <?php the_time('F jS, Y') ?></h4>-->

					<?php the_content(__('(more...)')); ?>
					<hr>
					<?php endwhile; else: ?>

					<?php _e('<p>Sorry, no posts matched your criteria.</p>'); ?>
				<?php endif; ?>

			<div class="navigation">
				<div id="alignright"><?php previous_posts_link( 'Newer Entries &raquo;' ); ?></div>
				<div id="alignleft"><?php next_posts_link( '&laquo; Older Entries', '' ); ?></div>
				<div id="clearingBlock"></div>
			</div>

		</div>

		<footer>
			<?php get_footer(); ?>
		</footer>

	</body>
</html>
