<p>Copyright&#169; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
<p>Vist Us on <a href="https://www.facebook.com/NHFchurch/">Facebook</a>.</p>
<p><a href="<?php bloginfo('rss2_url'); ?>" title="<?php _e('Syndicate this site using RSS'); ?>" class="feed"><?php _e('Entries <abbr title="Really Simple Syndication">RSS</abbr>'); ?></a>| <a href="http://wordpress.org/">Powered By Wordpress</a></p>
<p><?php wp_footer(); ?></p>